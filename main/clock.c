#include "clock.h"
#include "common.h"
#include "display.h"
#include "json.h"

#include "freertos/FreeRTOS.h"
#include "driver/hw_timer.h"
#include "freertos/task.h"

#define NUM_ROWS	(CONFIG_NUM_ROWS)
#define NUM_COLS	(CONFIG_NUM_COLS)
#define NUM_CHARS	(CONFIG_NUM_CHARS)
#define NUM_WIDTH	(NUM_CHARS * NUM_COLS)
#define TIMEOUT		(CONFIG_WIFI_TIMEOUT)

static struct clock {
	int s;
	int m;
	int h;
	int updated;
	int event_check;
	int inited;
} time;

static void seconds_tick(void) {
	time.s++;
	if (time.s > 59) {
		time.s = 0;
		time.m++;
		if (time.m > 59) {
			time.m++;
			time.h++;
			if (time.h > 23)
				time.h = 0;
		}
		if (!time.s)
			time.event_check = 1;
		time.updated = 1;
	}
}

void set_time(int h, int m, int s) {
	if ((time.h == h) && (time.m == m) && (time.s == s))
		return;
	time.h = h;
	time.m = m;
	time.s = s;
	time.updated = 1;
}

static void hw_timer_callback(void *arg) {
	seconds_tick();
}

char line[NUM_ROWS - 1][128] = { 0 };
int line_off[NUM_ROWS - 1] = { 0 };

char *line_str(int i) {
	if (i >= (NUM_ROWS - 1))
		return 0;
	memset(line[i], 0, 128);
	return line[i];
}

void dump_framebuffer(int row, unsigned char *str, int len) {
	debug("|");
	for (int i = 0; i < NUM_COLS * NUM_CHARS; i++) {
		int dot = !!(str[i] & 0x80);
		char c = str[i] & 0x7f;
		debug("%c%c|", c ? c : ' ', dot ? '.' : ' ');
	}
	debug("\n");
}

static void clock_worker(void){
	int i, j;
	if (!time.inited) {
		if (time.s == TIMEOUT) {
			clear_display();
			set_time(0, 0, 0);
			time.inited = 1;
			time_req();
			weather_req();
			return;
		}
		for (i = 0; i < NUM_ROWS; i++) {
			unsigned char buf[NUM_WIDTH] = { 0 };
			char str[NUM_WIDTH] = { 0 };
			switch(i) {
			case 0 :
				sprintf(str, "  %02d", TIMEOUT - time.s);
				break;
			case 1 :
				snprintf(str, NUM_WIDTH, "%s",
					CONFIG_WIFI_SSID);
				break;
			case 2 : 
				snprintf(str, NUM_WIDTH, "%s",
					CONFIG_WIFI_PASSWORD);
				break;
			}
			display_strcpy(buf, str, sizeof(buf), strlen(str));
			dump_framebuffer(i, buf, NUM_CHARS * NUM_COLS);
			for (j = 0; j < NUM_COLS; j++) {
				int off, slen;
				off = (NUM_WIDTH * i) + (j * NUM_CHARS);
				slen = (NUM_WIDTH) - (j * NUM_CHARS);
				put_string(off, buf + (j * NUM_CHARS), slen,
					NUM_CHARS);
			}
		}
		return;
	}
	for (i = 0; i < NUM_ROWS; i++) {
		unsigned char buf[NUM_WIDTH] = { 0 };
		if (time.event_check) {
			if (!time.h) {
				time_req();
				weather_req();
			}
			time.event_check = 0;
		}
		if (!i) {
			char str[NUM_WIDTH] = { 0 };

			sprintf(str, "%02d.%02d.%02d",
				time.h, time.m, time.s);
			display_strcpy(buf, str, sizeof(buf), strlen(str));
		} else {
			int curr_len, off = line_off[i - 1];
			char *curr = line[i - 1];

			curr_len = strlen(curr + off);
			display_strcpy(buf, curr + off, NUM_WIDTH, curr_len);
			off = off + 1;
			off += (curr[off] == '.') ? 1 : 0;
			if ((off >= strlen(curr)) || (off > 128))
				off = 0;
			line_off[i - 1] = off;
		}
		dump_framebuffer(i, buf, NUM_WIDTH);
		for (j = 0; j < NUM_COLS; j++) {
			int off, slen;
			off = (NUM_WIDTH * i) + (j * NUM_CHARS);
			slen = NUM_WIDTH - (j * NUM_CHARS);
			put_string(off, buf + (j * NUM_CHARS), slen,
				NUM_CHARS);
		}
	}
}

static void clock_task(void *pvParameter) {
	time.event_check = 0;
	time.inited = 0;
	while (1) {
		clock_worker();
		vTaskDelay(500 / portTICK_RATE_MS);
	}
}

int init_clock(void) {
	set_time(0, 0, 0);
        hw_timer_init(hw_timer_callback, NULL);
        hw_timer_alarm_us(1000000, 1);
	xTaskCreate(&clock_task, "clock_task", 0x1000, NULL, 1, NULL);
	return 0;
}

int stop_clock(void) {
        hw_timer_deinit();
	return 0;
}
