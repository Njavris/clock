#include "http.h"
#include "common.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include <netdb.h>
#include <sys/socket.h>
#include "string.h"
#include "esp_system.h"

#define MAX_CNT		10

static int dns_lookup(struct http_request *req, struct addrinfo **addr) {
	struct in_addr *in_addr;
	const struct addrinfo hints = {
		.ai_family = AF_INET,
		.ai_socktype = SOCK_STREAM,
	};
	debug("DNS lookp for \"%s\"\n", req->addr);
	if (getaddrinfo(req->addr, "80", &hints, addr))
		return -1;
	if (!(*addr))
		return -2;

	in_addr = &((struct sockaddr_in *)(*addr)->ai_addr)->sin_addr;
	debug("%s: IP=%s\n", req->addr, inet_ntoa(in_addr));
	return 0;
}

int http_request(struct http_request *req) {
	int i, sckt, rcv, head = 1, rcv_len = 0;
	struct addrinfo *addr;
	struct timeval tout;
	char recv_buf[RECV_LEN];
	
	for (i = 0; i < MAX_CNT; i++) {
		if (!dns_lookup(req, &addr))
			break;
		vTaskDelay(1000 / portTICK_PERIOD_MS);
	}
	if (i == MAX_CNT)
		return -1;

	sckt = socket(addr->ai_family, addr->ai_socktype, 0);
	if(sckt < 0) {
		debug("Failed to open socket\n");
		freeaddrinfo(addr);
		return -2;
	}

	if(connect(sckt, addr->ai_addr, addr->ai_addrlen) != 0) {
		debug("Failed to connect socket, err:%d\n", errno);
		close(sckt);
		freeaddrinfo(addr);
		return -3;
	}
	freeaddrinfo(addr);

	if (write(sckt, req->header, strlen(req->header)) < 0) {
		debug("Failed to write socket\n");
		close(sckt);
		return -4;
	}

	tout.tv_sec = 5;
	tout.tv_usec = 0;
	if (setsockopt(sckt, SOL_SOCKET,
			SO_RCVTIMEO, &tout, sizeof(tout)) < 0) {
		close(sckt);
		return -5;
	}

	if (req->start_cb)
		req->start_cb();
	do {
		memset(recv_buf, 0, RECV_LEN);
		for (i = 0; i < 10; i++) {
			rcv = read(sckt, recv_buf, sizeof(recv_buf)-1);
			if (rcv == -1 && errno == EAGAIN)
				continue;
			else
				break;
		}

		if (head) {
			head = 0;
			if(!strstr(recv_buf, "HTTP/1.1 200 OK")) {
				debug("HTTP request failed %s\n", recv_buf);
				return -6;
			}
		}
		if (req->data_cb)
			req->data_cb(recv_buf, rcv);
		rcv_len += rcv;
	} while(rcv > 0);
	if (rcv < 0)
		debug("read failed, err:%d\n", errno);

	if (req->done_cb)
		req->done_cb();
	close(sckt);
	debug("Recieved %d bytes\n", rcv_len);
	return 0;
}

void dummy_start_cb(void) {
	debug("================START==============\n")
}

void dummy_done_cb(void) {
	debug("\n");
	debug("=================END===============\n")
}

int dummy_data_cb(char *buf, int len) {
	int i;
	for(i = 0; i < len; i++) {
		if (buf[i] == '\r' && buf[i + 1] == '\n') {
			debug("\\r\\n%c%c", buf[i], buf[i + 1]);
			i ++;
		} else {
			debug("%c", buf[i]);
		}
	}
	return 0;
}

int init_http_request(struct http_request *req, char *addr, char *url,
		void (*start_cb)(void), void (*done_cb)(void),
		int (*data_cb)(char *, int)) {
	int url_len = strlen(url);
	if (!addr || !url_len)
		return -1;

	memset(req, 0, sizeof(struct http_request));
	req->addr = addr;
	snprintf(req->header, HDR_LEN,
		"GET %s HTTP/1.0\r\n"
		"Host: %s\r\n"
		"User-Agent: nuffin (FREERTOS)\r\n\r\n",
		url, addr);

	if (start_cb)
		req->start_cb = start_cb;
	if (done_cb)
		req->done_cb = done_cb;
	if (data_cb)
		req->data_cb = data_cb;

	req->inited = 1;
	return 0;
}
