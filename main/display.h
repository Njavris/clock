#ifndef __DISPLAY_H__
#define __DISPLAY_H__


struct display_dev {
	int cs;
	int data;
	int clk;
};

void put_string(int off, unsigned char *str, int slen, int wlen);
void clear_display(void);
void set_display(int display, int on);
void init_display(struct display_dev *dev);
int display_strcpy(unsigned char *dst, char *src, int dlen, int slen);

#endif
