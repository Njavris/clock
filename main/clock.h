#ifndef __CLOCK_H__
#define __CLOCK_H__

char *line_str(int i);
int init_clock(void);
void set_time(int h, int m, int s);
int stop_clock(void);

#endif
