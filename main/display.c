#include "display.h"
#include "common.h"
#include "config.h"
#include "clock.h"

#include "esp8266/pin_mux_register.h"
#include "esp8266/gpio_struct.h"
#include "rom/ets_sys.h"
#include "driver/gpio.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"

#include "ctype.h"
#include "string.h"

struct display_dev display_dev;

#define NUM_CHIPS	CONFIG_NUM_CHIPS
#define NUM_CHARS	CONFIG_NUM_CHARS
#define NUM_DIGITS	(NUM_CHIPS * NUM_CHARS)

struct display_char {
	char c;
	uint8_t value;
};

struct display_char font[] = {
	{' ', 0b00000000},
	{'0', 0b01111110},
	{'1', 0b00110000},
	{'2', 0b01101101},
	{'3', 0b01111001},
	{'4', 0b00110011},
	{'5', 0b01011011},
	{'6', 0b01011111},
	{'7', 0b01110000},
	{'8', 0b01111111},
	{'9', 0b01111011},
	{'-', 0b00000001},
	{'a', 0b01111101},
	{'b', 0b00011111},
	{'c', 0b00001101},
	{'d', 0b00111101},
	{'e', 0b01101111},
	{'f', 0b01000111},
	{'g', 0b01111011},
	{'h', 0b00010111},
	{'i', 0b00000100},
	{'j', 0b00011000},
	{'k', 0b01010111},
	{'l', 0b00000110},
	{'m', 0b00010100},
	{'n', 0b00010101},
	{'o', 0b00011101},
	{'p', 0b01100111},
	{'q', 0b01110011},
	{'r', 0b00000101},
	{'s', 0b01011011},
	{'t', 0b00001111},
	{'u', 0b00011100},
	{'v', 0b00011100},
	{'w', 0b00010100},
	{'x', 0b00110111},
	{'y', 0b00111011},
	{'z', 0b01101101},
	{'[', 0b01001110},
	{']', 0b01111000},
	{'_', 0b00001000},
};

void send_data(uint8_t addr, uint8_t data) {
	uint16_t buf = ((addr & 0xf) << 8) | (data & 0xff);
	for (int i = 0xf; i >= 0; i--) {
		gpio_set_level(display_dev.data, ((buf >> i) & 1) ? 1 : 0);
//		vTaskDelay(10 / portTICK_RATE_MS);
		gpio_set_level(display_dev.clk, 1);
//		vTaskDelay(10 / portTICK_RATE_MS);
		gpio_set_level(display_dev.clk, 0);
//		vTaskDelay(10 / portTICK_RATE_MS);
	}	
}

void send_cmd(int chip, uint8_t addr, uint8_t data) {
	gpio_set_level(display_dev.cs, 0);
	for (int i = 0; i < NUM_CHIPS; i++) {
		if (chip == (NUM_CHIPS - i - 1))
			send_data(addr, data);
		else
			send_data(0, 0);
	}
	gpio_set_level(display_dev.cs, 1);
}

void put_char(int chip, int pos, unsigned char ch) {
	int dot = (ch >> 8) & 1;
	if (!ch)
		ch = ' ';
	for (int i = 0; i < sizeof(font)/sizeof(struct display_char); i++) {
		if (font[i].c == tolower((int)ch)) {
			send_cmd(chip, NUM_CHARS - pos,
				font[i].value | (dot << 8));
			return;
		}
	}
	send_cmd(chip, NUM_CHARS - pos, 0); // blank if char isn't found
}

void put_string(int off, unsigned char *str, int slen, int wlen) {
	int limit = (wlen > 0) ? wlen : NUM_DIGITS;
	if (limit + off > NUM_DIGITS)
		limit = NUM_DIGITS - off;

	for (int i = 0; i < limit; i++) {
		int dot = (str[i] >> 8) & 1; 
		unsigned char c = (i < slen) ? str[i] : ' ' | (dot << 8);
		put_char((i + off) / NUM_CHARS, (i + off) % NUM_CHARS, c);
	}
}

void clear_display(void) {
	for (int i = 0; i < NUM_DIGITS; i++)
		put_char(i / NUM_CHARS, i % NUM_CHARS, ' ');
}

void set_display(int display, int on) {
	if (on)
		send_cmd(display, 0xc, 1);
	else
		send_cmd(display, 0xc, 0);
}

int display_strcpy(unsigned char *dst, char *src, int dlen, int slen){
	int i, ii = 0;
	for (i = 0; (i < dlen) && (ii < slen); i++) {
		int dot = src[ii + 1] == '.';
		dst[i] = src[ii] | (dot << 7);
		if (dot)
			ii++;
		ii++;
	}
	return ii;
}

void init_display(struct display_dev *dev) {
	memcpy(&display_dev, dev, sizeof(display_dev));

	gpio_config_t io_conf;
	io_conf.intr_type = GPIO_INTR_DISABLE;
	io_conf.mode = GPIO_MODE_OUTPUT;
	io_conf.pin_bit_mask = (1ULL << display_dev.cs) | 
				(1ULL << display_dev.data) |
				(1ULL << display_dev.clk);
	io_conf.pull_down_en = 0;
	io_conf.pull_up_en = 0;
	gpio_config(&io_conf);

	gpio_set_level(display_dev.cs, 1);
	gpio_set_level(display_dev.clk, 0);

	for (int i = 0; i < NUM_CHIPS; i++) {
		send_cmd(i, 0xf, 0);
		//send_cmd(i, 0x9, 0xff); // b mode font
		send_cmd(i, 0x9, 0); // no font
		send_cmd(i, 0xb, 0x7);
		send_cmd(i, 0xa, 8);
		send_cmd(i, 0xc, 0);
	}
}
