#ifndef __CONFIG_H__
#define __CONFIG_H__

enum cfg {
	CFG_WIFI_SSID = 0,
	CFG_WIFI_PWD,
	CFG_TIMEZONE,
	CFG_WEATHER_LAT,
	CFG_WEATHER_LONG,
	CFG_WEATHER_API_KEY,
	CFG_MAX,
};

void *get_cfg(enum cfg c);
void set_cfg(enum cfg c, void *val);
#endif
