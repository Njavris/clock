#ifndef __HTTP_H__
#define __HTTP_H__

#define HDR_LEN		512
#define RECV_LEN	256

struct http_request {
	char *addr;
	char header[HDR_LEN];
	void (*start_cb)(void);
	void (*done_cb)(void);
	int (*data_cb)(char *, int);
	int inited;
};

int init_http_request(struct http_request *req, char *addr, char *url,
		void (*start_cb)(void), void (*done_cb)(void),
		int (*data_cb)(char *, int));
int http_request(struct http_request *req);
void dummy_start_cb(void);
void dummy_done_cb(void);
int dummy_data_cb(char *buf, int len);
void init_http(void);

#endif
