#ifndef __COMMON_H__
#define __COMMON_H__

#include <string.h>

typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
#define DEBUG
#ifdef DEBUG
#include <stdio.h>
#define debug(...)      printf(__VA_ARGS__);
#else
#define debug(...)
#endif

#endif
