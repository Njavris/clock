#include "config.h"
#include "common.h"

#include "esp_system.h"

static void *configs[CFG_MAX];

void *get_cfg(enum cfg c) {
	if (c > CFG_MAX)
		return NULL;
	else
		return configs[c];
}


void set_cfg(enum cfg c, void *val) {
	if (c > CFG_MAX)
		return;
	else
		configs[c] = val;
}
