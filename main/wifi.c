#include "wifi.h"
#include "common.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_netif.h"
#include "esp_event.h"
#include "esp_wifi.h"
#include "lwip/err.h"
#include "lwip/sys.h"

static EventGroupHandle_t s_wifi_event_group;

#define WIFI_CONNECTED_BIT BIT0

static void event_handler(void* arg, esp_event_base_t event_base,
				int32_t event_id, void* event_data) {
	if (event_base == WIFI_EVENT) {
		if (event_id == WIFI_EVENT_STA_START) {
			esp_wifi_connect();
		} else if (event_id == WIFI_EVENT_STA_DISCONNECTED) {
			esp_wifi_connect();
			debug("retry to connect to the AP\n");
		}
	} else if (event_base == IP_EVENT) {
		if (event_id == IP_EVENT_STA_GOT_IP) {
			ip_event_got_ip_t *event =
				(ip_event_got_ip_t *)event_data;
			xEventGroupSetBits(s_wifi_event_group,
				WIFI_CONNECTED_BIT);
		}
	}
}

int wifi_sta_init(char *ssid, char *pwd) {
	s_wifi_event_group = xEventGroupCreate();
	tcpip_adapter_init();
	esp_event_loop_create_default();
	wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
	esp_wifi_init(&cfg);
	esp_event_handler_register(WIFI_EVENT, ESP_EVENT_ANY_ID, &event_handler, NULL);
	esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP, &event_handler, NULL);

	wifi_config_t wifi_config;
	memset(&wifi_config, 0, sizeof(wifi_config));
	if ((strlen(ssid) <= 0) && (strlen(pwd) <= 0))
		return -1;
	strncpy((char *)wifi_config.sta.ssid, ssid, 32);
	strncpy((char *)wifi_config.sta.password, pwd, 64);
	if (strlen(pwd))
		wifi_config.sta.threshold.authmode = WIFI_AUTH_WPA2_PSK;
	debug("trying SSID: %s PWD: %s\n",
			(char *)wifi_config.sta.ssid,
			(char *)wifi_config.sta.password);

	esp_wifi_set_mode(WIFI_MODE_STA);
	esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config);
	esp_wifi_start();

	EventBits_t bits = xEventGroupWaitBits(s_wifi_event_group,
			WIFI_CONNECTED_BIT,
			pdFALSE,
			pdFALSE,
			portMAX_DELAY);

	if (bits & WIFI_CONNECTED_BIT) {
		debug("connected to ap\n");
	} else {
		debug("unexpected\n");
		return -2;
	}
	return 0;
}

int wifi_sta_deinit(void) {
	esp_event_handler_unregister(IP_EVENT, IP_EVENT_STA_GOT_IP,
					&event_handler);
	esp_event_handler_unregister(WIFI_EVENT, ESP_EVENT_ANY_ID,
					&event_handler);
	vEventGroupDelete(s_wifi_event_group);
	return 0;
}
