#include "json.h"
#include "common.h"
#include "config.h"
#include "http.h"
#include "clock.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#define DATA_LEN	(15 * 1024)
#define	FORECAST_LEN	24

static struct JSON_reader {
	int in_progress;
	int recv_len;
	struct http_request *req;
	char data[DATA_LEN];
	char *json_data;
} json;

enum {
	REQ_TIME = 0,
	REQ_WEATHER,
	REQ_MAX,
};

struct http_request reqs[REQ_MAX];

static void json_start_cb(void) {
	json.in_progress = 1;
	json.recv_len = 0;
	memset(json.data, 0, DATA_LEN);
//	debug("================START==============\n")
}

static void json_done_cb(void) {
	json.in_progress = 0;
	json.json_data = json.data;
//	debug("\n\n%s\n", json.json_data);
//	debug("=================END===============\n")
	debug("Handled JSON was %d bytes long\n", json.recv_len);
}

// TODO: something more efficient than 15kb buffer is needed
static int json_data_cb(char *buf, int len) {
	memcpy(&json.data[json.recv_len], buf, len);
	json.recv_len += len;
	return 0;
}

static int do_json_req(void) {
	return http_request(json.req);
}

static void time_start_cb(void) {
	json_start_cb();
}

static void time_done_cb(void) {
	char *datetime, *end;
	char date[10] = { 0 };
	int t[3], i;

	json_done_cb();
	memset(date, 0, sizeof(date));
	datetime = strstr(json.json_data, "\"datetime\":\"");
	if (!datetime)
		return;
	datetime += strlen("\"datetime\":\"") + 2;	
	
	end = strchr(datetime, 'T');
	memcpy(date, datetime, (unsigned)end - (unsigned)datetime);
	datetime = end + 1;

	debug("date: %s\n", date);
	for (i = 0; i < 3; i++) {
		end = strchr(datetime, ':');
		if (!end)
			return;
		t[i] = atoi(datetime);
		datetime = end + 1;
	}
	set_time(t[0], t[1], t[2]);
}

static int time_data_cb(char *buf, int len) {
	return json_data_cb(buf, len);
}

static void init_time_req(void) {
	char addr[128] = { 0 };
	sprintf(addr, "http://worldtimeapi.org/api/timezone/%s",
		(char*)get_cfg(CFG_TIMEZONE)); 
	init_http_request(json.req, "worldtimeapi.org",
		addr,
		time_start_cb,
		time_done_cb,
		time_data_cb);
}

int time_req(void) {
	json.req = &reqs[REQ_TIME];
	if (!json.req->inited)
		init_time_req();
	return do_json_req();
}

static void weather_start_cb(void) {
	json_start_cb();
}

static int find_key_val(char *str, char *name, char *dst, int dst_len) {
	char *start, *end, s = ',';
	int len;
	if (!str)
		return 0;
	start = strstr(str, name);
	if (!start)
		return 0;
	start += strlen(name) + 1;
	if (start[0] == '\"') {
		s = '\"';
		start++;
	}
	end = strchr(start, s);
	if (!end)
		return 0;
	str = start;
	len = (int)((unsigned)end - (unsigned)start);
	memset(dst, 0, dst_len);
	memcpy(dst, start, len < dst_len ? len : dst_len);
	return len;
}

struct weather_buf {
	char time[8];
	char temp[10];
	char feels_like[10];
	char wind_speed[10];
	char clouds[4];
	char description[64];
};

// TODO: change to not waste 15kb on packet storage
static void weather_done_cb(void) {
	char *start = json.json_data, *end, buf[16];
	unsigned timezone_offset;
	int r;
	json_done_cb();
	r = find_key_val(start, "\"timezone_offset\"", buf, 16);
	if (!r)
		return;
	timezone_offset = atoi(buf);
	end = strstr(json.json_data, "{\"dt\"");
	for (int i = 0; i < FORECAST_LEN; i++) {
		struct tm lt;
		unsigned dt;
		char *str;
		start = end;
		end = strstr(start + 1, "{\"dt\"");
		if (!end)
			break;
		if (i % 3)
			continue;
		str = line_str(i/3);
		if (!str)
			continue;

		r = find_key_val(start, "\"dt\"", buf, 16);
		if (!r)
			return;
		dt = atoi(buf) + timezone_offset;
		localtime_r(&dt, &lt);
		strftime(str, 5, "%H%M", &lt); 
		str += strlen(str);
		strcpy(str, " temp ");
		str += strlen(" temp ");
		str += find_key_val(start, "\"temp\"", str, 10);
		strcpy(str, "[");
		str += strlen("[");
		str += find_key_val(start, "\"feels_like\"", str, 10);
		strcpy(str, "]C wind ");
		str += strlen("]C wind ");
		str += find_key_val(start, "\"wind_speed\"", str, 10);
		strcpy(str, "ms clouds ");
		str += strlen("ms clouds ");
		str += find_key_val(start, "\"clouds\"", str, 4);
		strcpy(str, " ");
		str += strlen(" ");
		str += find_key_val(start, "\"description\"", str, 64);
	}
}

static int weather_data_cb(char *buf, int len) {
	return json_data_cb(buf, len);
}

static void init_weather_req(void) {
	char addr[256] = { 0 };
	sprintf(addr, "http://api.openweathermap.org/data/2.5/onecall?"
		"lat=%s&lon=%s&exclude=current,minutely,daily,alerts"
		"&units=metric&appid=%s",
		(char*)get_cfg(CFG_WEATHER_LAT),
		(char*)get_cfg(CFG_WEATHER_LONG),
		(char*)get_cfg(CFG_WEATHER_API_KEY));
	init_http_request(json.req, "api.openweathermap.org",
		addr,
		weather_start_cb,
		weather_done_cb,
		weather_data_cb);
}

int weather_req(void) {
	json.req = &reqs[REQ_WEATHER];
	if (!json.req->inited)
		init_weather_req();
	return do_json_req();
}
