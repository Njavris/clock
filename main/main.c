#include "esp_spi_flash.h"

#include "common.h"
#include "config.h"
#include "wifi.h"
#include "display.h"
#include "http.h"
#include "json.h"
#include "clock.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_spi_flash.h"
#include "nvs.h"
#include "nvs_flash.h"

static void debug_task(void *pvParameter) {
	debug("69:69:69");	
	while (1) {
		vTaskDelay(1000 / portTICK_RATE_MS);
	}
}

void app_main()
{
	struct display_dev display;
	set_cfg(CFG_WIFI_SSID, (void*)CONFIG_WIFI_SSID);
	set_cfg(CFG_WIFI_PWD, (void*)CONFIG_WIFI_PASSWORD);
	set_cfg(CFG_TIMEZONE, (void*)CONFIG_TIMEZONE);
	set_cfg(CFG_WEATHER_LAT, (void*)CONFIG_WEATHER_LAT);
	set_cfg(CFG_WEATHER_LONG, (void*)CONFIG_WEATHER_LONG);
	set_cfg(CFG_WEATHER_API_KEY, (void*)CONFIG_WEATHER_API_KEY);

	esp_chip_info_t chip_info;
	esp_chip_info(&chip_info);
	debug("This is ESP8266 chip with %d CPU cores, WiFi, ",
			chip_info.cores);
	debug("silicon revision %d, ", chip_info.revision);
	debug("%dMB %s flash\n", spi_flash_get_chip_size() / (1024 * 1024),
			(chip_info.features & CHIP_FEATURE_EMB_FLASH) ? 
			"embedded" : "external");

	nvs_flash_init();
	char *ssid = (char*)get_cfg(CFG_WIFI_SSID);
	char *pwd = (char*)get_cfg(CFG_WIFI_PWD);    
	wifi_sta_init(ssid, pwd);

	init_clock();

#ifdef DEBUG
//	xTaskCreate(&debug_task, "debug_task", 0x400, NULL, 1, NULL);
#endif

	display.cs = CONFIG_CS_GPIO;
	display.data = CONFIG_DATA_GPIO;
	display.clk = CONFIG_CLK_GPIO;
	init_display(&display);

//	fflush(stdout);
//	esp_restart();
}
